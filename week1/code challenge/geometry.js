/*function balok(panjang, lebar, tinggi) {
  return panjang * lebar * tinggi;
}

function tabung(vi, r2, tinggi1) {
  return vi * r2 * tinggi1;
}

var volumebalok = balok(235, 129, 74);
var volumetabung = tabung(3.14, 400, 74);

function volume() {
  console.log(volumebalok);
  console.log(volumetabung);
  console.log(volumebalok + volumetabung);
}

volume();*/

/* If we use callback function, we can't add another logic outside the function */

// Import readline
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// Function to calculate area of ​​rectangle
function rectangle(length, height) {
  return length * width;
}

/* Way 1 */
// Function for inputing length of beam
/*function inputLength() {
  rl.question(`Length: `, (length) => {
    if (!isNaN(length)) {
      inputWidth(length);
    } else {
      console.log(`Length must be a number\n`);
      inputLength();
    }
  });
}

// Function for inputing width of beam
function inputWidth(length) {
  rl.question(`Width: `, (width) => {
    if (!isNaN(width)) {
      inputHeight(length, width);
    } else {
      console.log(`Width must be a number\n`);
      inputWidth(length);
    }
  });
}

// Function for inputing height of beam
function inputHeight(length, width) {
  rl.question(`Height: `, (height) => {
    if (!isNaN(height)) {
      console.log(`\nBeam: ${beam(length, width, height)}`);
      rl.close();
    } else {
      console.log(`Height must be a number\n`);
      inputHeight(length, width);
    }
  });
}
/* End Way 1 */

/* Alternative Way */
// All input just in one code
function inputHeight() {
  rl.question("Length: ", function (length) {
    console.log(length);
    rl.question("Height: ", (height) => {
      if (!isNaN(length) && !isNaN(height)) {
        console.log("Area of rectangle is " + length * height);
        rl.close();
      } else {
        console.log(`Length, and Height must be a number\n`);
        inputHeight();
      }
    });
  });
}
/* End Alternative Way */

console.log(`Are of Rectangle`);
console.log(`Please input the number`);
// inputLength(); // Call way 1
inputHeight(); // Call Alternative Way
