function input() {
  index.rl.question("Length: ", (length) => {
    index.rl.question("Width: ", (width) => {
      index.rl.question("Height: ", (height) => {
        if (!isNaN(length) && !isNaN(width) && !isNaN(height)) {
          console.log(`\nBeam: ${beam(length, width, height)} \n`);
          index.rl.close();
        } else {
          console.log(`Length, Width and Height must be a number\n`);
          input();
        }
      });
    });
  });
}
