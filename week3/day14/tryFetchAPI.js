const axios = require("./axios");
const url = "https://jsonplaceholder.typicode.com/posts";

//promise

axios.get(url).then((Response) => {
  console.log(Response.data);
});
