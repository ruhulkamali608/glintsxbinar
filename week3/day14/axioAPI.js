const axios = require("axios");
const NewsGermany = "https://newsapi.org/v2/top-headlines?country=de&apiKey=b0df378848ad477a84fb48978bd68f2d";
const NewsIndonesia = "https://newsapi.org/v2/top-headlines?country=id&apiKey=b0df378848ad477a84fb48978bd68f2d";
const NewsCanada = "https://newsapi.org/v2/top-headlines?country=ca&apiKey=b0df378848ad477a84fb48978bd68f2d";
//promise

axios.get(NewsGermany).then((Response) => {
  console.log(Response.data);
});

axios.get(NewsIndonesia).then((Response) => {
  console.log(Response.data);
});

axios.get(NewsCanada).then((Response) => {
  console.log(Response.data);
});

//async await
async function makeGetRequestGermany() {
  let res = await axios.get(NewsGermany);
  let data = res.data;
  console.log(data);
}

async function makeGetRequestIndonesia() {
  let res = await axios.get(NewsIndonesia);
  let data = res.data;
  console.log(data);
}

async function makeGetRequestIndonesia() {
  let res = await axios.get(NewsCanada);
  let data = res.data;
  console.log(data);
}

async function makeGetRequestCanada() {
  let res = await axios.get(NewsGermany);
  let data = res.data;
  console.log(data);
}

makeGetRequestGermany();
makeGetRequestIndonesia();
makeGetRequestCanada();

