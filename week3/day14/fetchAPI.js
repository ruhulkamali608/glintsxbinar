const fetch = require('node-fetch');

const url = "https://newsapi.org/v2/top-headlines?country=id&apiKey=b0df378848ad477a84fb48978bd68f2d";

//Promise
fetch(url)
  .then((res) => res.json())
  .then((json) => console.log(json));

// Async await
async function fetchAPI() {
  const response = await fetch(url);
  const data = await response.json();

  console.log(data);
}
fetchAPI();
