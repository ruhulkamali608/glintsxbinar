//arrow function
const adderArrowReturn = (x, y) => x * y;

const adderArrow = (x, y) => {
  return x * y;
};

const adderConsole = (x, y) => {
  console.log(`${x * y}`);
};

function adderFunc(x, y) {
  return x * y;
}

const adder1 = adderArrowReturn(10, 11);
const adder2 = adderArrow(10, 11);
const adder3 = adderFunc(10, 11);

adderConsole(10, 11);
console.log(`${adder1}, ${adder2}, ${adder3}`);

// Currying fucntion //

const substracCurryReturn = (x) => (y) => {
  return x - y;
};

const substracCurryConsole = (x) => (y) => {
  console.log(x - y);
};

function substractFunc(x, y) {
  return x - y;
}

const substrac1 = substracCurryReturn(10)(11);
const substrac2 = substracCurryConsole(10)(11);
const substrac3 = substractFunc(10, 11);

substracCurryConsole(10)(11)(12);
console.log(`${substrac1}, ${substrac2}, ${substrac3}`);

//glt
