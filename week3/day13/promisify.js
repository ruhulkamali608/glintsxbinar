const { promisify } = require("util");
const fs = require("fs");

const readFile = promisify(fs.readFile);

readFile("./data/david.txt", "utf-8").then((david) => {
  console.log(david);

  return readFile("./data/kim.txt", "utf-8");
});

//belum slesai
