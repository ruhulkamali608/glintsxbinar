import { v4 as uuidv4 } from "uuid";

let users = [
  {
    username: "John doe",
    hobby: "fishing and killing people",
    desire: "Break the rule and Blow up the world",
    //will get automatic id when create new user
    id: "67eee9b1-b688-4f18-a7bd-999629d0a48c",
  },

  {
    username: "Ruhul kamali",
    hobby: "listening music,create art and chill",
    desire: "Make the world more better and beutiful for all creatures live",
    //will get automatic id when create new user
    id: "514d7edf-4714-47ca-b893-430f19c10a83",
  },
];

export const getUsers = (req, res) => {
  res.send(users);
};

export const createUser = (req, res) => {
  const user = req.body;

  users.push({ ...user, id: uuidv4() });

  res.send(`User [${user.username}] added to the database!`);
};

export const getUser = (req, res) => {
  const { id } = req.params;

  const foundUser = users.find((user) => user.id === id);

  res.send(foundUser);
};

export const deleteUser = (req, res) => {
  const { id } = req.params;

  users = users.filter((user) => user.id !== id);

  res.send(`user with id ${id} has been deleted`);
};

export const updateUser = (req, res) => {
  const { id } = req.params;
  const { username, hobby, desire } = req.body;

  const user = users.find((user) => user.id === id);

  if (username) user.username = username;
  if (hobby) user.hobby = hobby;
  if (desire) user.desire = desire;

  res.send(`User with ID ${id} has been updated`);
};
