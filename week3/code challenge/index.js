import express from "express";
import bodyParser from "body-parser";

import userRouters from "./routes/users.js";

const app = express();
const PORT = 5000;

app.use(bodyParser.json());

app.use("/users", userRouters);

app.get("/", (req, res) =>
  res.send(
    "Welcome to your own space\n Share your hobby and tell to world what is your really DESIRE!"
  )
);

app.listen(PORT, () =>
  console.log(`server running on port: http://localhost:${PORT}`)
);
