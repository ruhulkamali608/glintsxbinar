const Geometry = require("./Geometry");

class Dimention extends Geometry {
  constructor(name) {
    super(name, "Dimention");

    if (this.constructor === Dimention) {
      throw new Error("Dimenstion is abstract class");
    }
  }

  whoAmI() {
    super.whoAmI();
    console.log(`I'm ${this.type}`);
  }

  calculateCircumference() {
    console.log(
      `${this.name} Circumference!\n
      ============================\n`
    );
  }

  calculateArea() {
    console.log(`${this.name} Area!\n
      ===============================\n`);
  }
}

module.exports = Dimention;
