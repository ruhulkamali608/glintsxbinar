const Dimention = require(`./Dimention`);

class Square extends Dimention {
  constructor(width, length) {
    super("Square");
    this.width = width;
    this.leght = length;
  }

  whoAmI() {
    this.#whoareWe();
    console.log(`I'm ${this.name}`);
  }

  #whoareWe() {
    super.whoAmI;
  }

  calculateCircumference(whoAreYou) {
    super.calculateCircumference();
    const who = `${whoAreYou} is trying to calculate circumference: `;
    return `${who} ${2 * (this.width + this.leght)} cm`;
  }

  calculateArea(whoAreYou) {
    super.calculateArea();
    const who = `${whoAreYou} is trying to calculate are: `;
    return `${who} ${this.width * this.leght} cm2`;
  }
}

const square1 = new Square(12, 10);
square1.whoAmI();
console.log(square1.calculateCircumference("reza"));
console.log(square1.calculateArea("reza"));
