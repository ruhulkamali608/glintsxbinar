class animal {
  constructor(TypeOfAnimal) {
    this.TypeOfAnimal = TypeOfAnimal;
  }

  Type() {
    if (this.TypeOfAnimal === "reptiles")
      console.log(`${this.name} is a reptiles type animal!`);
  }
}

class mammals extends animal {
  constructor(name) {
    super("mammals");
    this.name = name;
  }
}

class bird extends animal {
  constructor(name) {
    super("bird");
    this.name = name;
  }
}

class reptiles extends animal {
  constructor(name) {
    super("reptiles");
    this.name = name;
  }
}

const snake = new reptiles("Cobra");
const Cow = new mammals("Chianina");
const chameleon = new reptiles("Panther Chameleon");
const birds = new bird("Pigeon");
const chiken = new bird("Rooster");

snake.Type();
Cow.Type();
chameleon.Type();
birds.Type();
chiken.Type();
