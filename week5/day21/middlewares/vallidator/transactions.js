const { ObjectId } = require("mongodb");
const validator = require("validator");
const connection = require("../../models");

exports.createTransactionValidator = async (req, res, next) => {
  try {
    const errorMessages = [];

    if (!validator.isInt(req.body.quantity)) {
      errorMessages.push("quantity must be integer");
    }

    if (errorMessages.length > 0) {
      return next({ messages: errorMessages, statusCode: 400 });
    }

    const good = await connection
      .db("sales_morning")
      .collection("good")
      .findOne({ _id: ObjectId(req.params.id_good) });

    if (!good) {
      return next({ message: "good not found", statusCode: 400 });
    }

    const total =
      parseFloat(req.body.quantity) * parseFloat(good.price.toString());

    req.body = {
      id_good: ObjectId(req.body.id_good),
      id_customer: ObjectId(req.body.id_customer),
      quantity: parseFloat(req.body.quantity),
      total,
    };
    next();
  } catch (error) {
    next(error);
  }
};
