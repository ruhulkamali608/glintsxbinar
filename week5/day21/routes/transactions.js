const express = require("express");
//const { Transaction } = require("mongodb");

const {
  createTransactionValidator,
} = require("../middlewares/vallidator/transactions");

const {
  createTransaction,
  getAllTransactions,
  getDetailTransaction,
  updateTransaction,
  deleteTransaction,
} = require("../controllers/transactions");

const router = express.Router();

router
  .route("/")
  .post(createTransactionValidator, createTransaction)
  .get(getAllTransactions);

router
  .route("/:id")
  .get(getDetailTransaction)
  .put(updateTransaction)
  .delete(deleteTransaction);

module.exports = router;
