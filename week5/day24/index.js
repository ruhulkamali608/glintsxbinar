require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
}); // Config environment

const express = require("express"); // import express
const fileUpload = require("express-fileupload");

const app = express(); // make express app

// Import routes
const transactions = require("./routes/transactions");
const errorHandler = require("./middlewares/errorHandler");

app.use(express.json()); // enable req.body JSON

app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use(fileUpload());

//make public folder as static

app.use(express.static("public"));
// Use routes

app.use("/transactions", transactions);

app.all("*", async (req, res, next) => {
  try {
    next({ message: "Endpoint is not found", statusCode: 404 });
  } catch (error) {
    next(error);
  }
});

app.use(errorHandler);
// Running server
app.listen(3000, () => console.log(`Server running on port 3000!`));
