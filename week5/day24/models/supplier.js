const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");

const supplierSchema = new mongoose.Schema(
  // for column
  {
    name: {
      type: String,
      required: true,
    },
    photo: {
      type: String,
      required: false,
      get: getPhoto,
    },
  },
  // Enable timestamps
  {
    timestamps: {
      createdAt: "createAt",
      updatedAt: "updatedAt",
    },
  }
);

// Get photo

function getPhoto(photo) {
  if (!photo) {
    return photo;
  }

  if (photo.includes("https") || photo.includes("http")) {
    return photo;
  }

  return `/images/suppliers/${photo}`;
}

// enable soft delete, it will make delete column automaticly
supplierSchema.plugin(mongooseDelete, { overrideMethods: "all" });

module.exports = mongoose.model("supplier", supplierSchema);
