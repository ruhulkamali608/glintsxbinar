const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");

const goodSchema = new mongoose.Schema(
  // for column
  {
    name: {
      type: String,
      required: true,
      unique: true,
    },

    price: {
      type: Number,
      required: true,
    },

    photo: {
      type: String,
      required: false,
      get: getPhoto,
    },
    supplier: {
      type: mongoose.Schema.ObjectId,
      require: true,
      ref: "supplier", // it will make likes join to supplier
    },
  },
  // Enable timestamps
  {
    timestamps: {
      createdAt: "createAt",
      updatedAt: "updatedAt",
    },
  }
);

// Get photo

function getPhoto(photo) {
  if (!photo) {
    return photo;
  }

  if (photo.includes("https") || photo.includes("http")) {
    return photo;
  }

  return `/images/goods/${photo}`;
}

// enable soft delete, it will make delete column automaticly
goodSchema.plugin(mongooseDelete, { overrideMethods: "all" });

module.exports = mongoose.model("good", goodSchema);
