const validator = require("validator");
const { good } = require("../../models");

exports.createTransactionValidator = async (req, res, next) => {
  try {
    const errorMessages = [];

    if (!validator.isInt(req.body.id_good)) {
      errorMessages.push("Quantity must be number (interger)");
    }

    if (!validator.isInt(req.body.quantity)) {
      errorMessages.push("Quantity must be number (interger)");
    }

    if (!validator.isInt(req.body.id_customer)) {
      errorMessages.push("Quantity must be number (interger)");
    }

    if (errorMessages.length > 0) {
      return next({ statusCode: 400, message: errorMessages });
    }

    const findGood = await good.findOne({ where: { id: req.body.id_good } });

    const price = findGood.price;
    req.body.total = eval(price * req.body.quantity);

    next();
  } catch (error) {
    next(error);
  }
};
