const express = require("express"); // import express

const app = express(); // make express app

// Import routes
const transactions = require("./routes/transactions");

const errorHandler = require("./middlewares/errorHandler");

app.use(express.json()); // enable req.body JSON

app.use(
  express.urlencoded({
    extended: true,
  })
);

// Use routes

app.use("/transactions", transactions);

// user err handler
app.use(errorHandler);
// Running server
app.listen(3000, () => console.log(`Server running on port 3000!`));
