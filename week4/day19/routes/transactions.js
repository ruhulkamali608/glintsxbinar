const express = require("express");

//validator
const {
  createTransactionValidator,
} = require("../middlewares/validators/transactions");

const { createTransaction } = require("../controllers/transactions");

const router = express.Router();

router.post("/", createTransactionValidator, createTransaction);

module.exports = router;
