const { transaction, good, supplier, customer } = require("../models");

class transactions {
  async createTransaction(req, res, next) {
    try {
      const newdata = await transaction.create(req.body);

      const data = await transaction.findOne({
        where: {
          id: newdata.id,
        },
        attributes: { exclude: ["id_good", "id_customer"] },

        include: [
          {
            model: good,
            include: [
              {
                model: supplier,
              },
            ],
          },
          {
            model: customer,
          },
        ],
      });

      res.status(201).json({ data });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = new transactions();
