const express = require("express"); // impert express

const {
  getAllTransactions,
  getDetailTransaction,
  createTransaction,
} = require("../controllers/transactions.js");

const router = express.Router(); // make express router

// if user (get) /transaction will go here
router.get("/", getAllTransactions);
router.get("/:id", getDetailTransaction);
router.post("/", createTransaction);

module.exports = router; // export router
