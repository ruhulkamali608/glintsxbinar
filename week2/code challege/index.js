const data = require("./lib/arrayFactory.js");
const test = require("./lib/test.js");

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter((i) => typeof i === "number");
}

// Should return array
function sortAscending(data) {
  const len = data.length;
  const swap = (data, right, left) =>
    ([data[right], data[left]] = [data[left], data[right]]);

  for (let i = 0; i < len; i++) {
    for (let j = 0; j < len - i - 1; j++) {
      if (data[j] > data[j + 1]) {
        swap(data, j, j + 1);
      }
    }
  }
  return data;
}

// Should return array
function sortDecending(data) {
  const len = data.length;
  const swap = (data, left, right) =>
    ([data[right], data[left]] = [data[left], data[right]]);

  for (let i = 0; i < len - 1; i++) {
    for (let j = 0; j < len; j++) {
      if (data[j] < data[j + 1]) {
        swap(data, j + 1, j);
      }
    }
  }

  return data;
}

// DON'T CHANGE
test(sortAscending, sortDecending, data);
