const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

console.log("Hello,Here is the list fresh food today");
console.log(1 + ":tomato");
console.log(2 + ":broccoli");
console.log(3 + ":kale");
console.log(4 + ":cabbage");
console.log(5 + ":apple");

const vegetable = ["", "tomato", "broccoli", "kale", "cabbage", "apple"];

rl.question("please input your vegetable number: ", (fruit) => {
  if (fruit <= 4) {
    console.log(
      vegetable[fruit] + "  is a healthy food, it's definitely worth to eat"
    );
    rl.close();
  } else if (fruit === 5) {
    console.log(vegetable[fruit] + " is not a vegetable");
    rl.close();
  } else {
    console.log("your input is not correct");
  }
});
